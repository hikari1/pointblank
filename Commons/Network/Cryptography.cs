﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Network
{
    public class Cryptography
    {
        public void Encrypt(ref byte[] data, int shift)
        {
            var length = data.Length;
            var first = data[0];
            byte current;
            for (int i = 0; i < length; i++)
            {
                current = i >= (length - 1) ? first : data[i + 1];
                data[i] = (byte)(current >> (8 - shift) | (data[i] << shift));
            }
        }
        public void Decrypt(ref byte[] data, int shift)
        {
            try
            {
                var length = data.Length;
                var last = data[length - 1];
                byte current;
                for (int i = length - 1; (i & 0x80000000) == 0; i--)
                {
                    current = i <= 0 ? last : data[i - 1];
                    data[i] = (byte)(current << (8 - shift) | data[i] >> shift);
                }
            }
            catch 
            {
                data = new byte[0];
            }
        }
    }
}
