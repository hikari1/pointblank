﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Network.Bootstrap
{
    public interface IBootstrap
    {
        Task Bind();
        Task Shutdown();
    }
}