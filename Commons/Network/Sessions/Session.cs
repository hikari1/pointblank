﻿using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Commons.Network.Sessions
{
    public class Session
    {
        public string Address => ((IPEndPoint) _channel.RemoteAddress).Address.MapToIPv4().ToString();;
        public Cryptography Cryptography { get; }
        private IChannel _channel { get; }
        public Session (IChannel channel, Cryptography cryptography)
        {
            Cryptography = cryptography;
            _channel = channel;
        }
        public void Disconnect()
        {
            _channel.DisconnectAsync();
        }

        public void Send()
        {
            if (!_channel.IsWritable) return;
        }
    }
}
