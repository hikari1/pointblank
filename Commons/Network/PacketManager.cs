﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Commons.Network
{
    public class PacketManager<T, R>
    {
        private ConcurrentDictionary<T, R> _packets = new ConcurrentDictionary<T, R>();
        public int Count => _packets.Count;

        public bool TryAdd(T code, R packetHandler)
        {
            return _packets.TryAdd(code, packetHandler);
        }

        public bool TryGet(T code, out R handler)
        {
            return _packets.TryGetValue(code, out handler);
        }
    }
}
