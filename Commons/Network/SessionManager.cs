﻿using Commons.Network.Sessions;
using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Commons.Network
{
    public class SessionManager
    {
        private object _lock = new Object();
        private List<Session> _sessions = new List<Session>();

        public void Add(Session session)
        {
            lock (_lock)
            {
                _sessions.Add(session);
            }
        }

        public void Remove(Session session)
        {
            lock (_lock)
            {
                _sessions.Remove(session);
            }
        }

        public Session FirstOrDefault(Func<Session, bool> expression)
        {
            return _sessions.Where(expression).FirstOrDefault();
        }
    }
}
