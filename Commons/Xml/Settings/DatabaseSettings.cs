﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Xml.Settings
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
