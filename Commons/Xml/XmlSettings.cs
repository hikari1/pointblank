﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Commons.Xml
{
    public class XmlSettings<T>
    {
        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(T));

        public T Serializer { get; private set; }

        public bool Load(string path, bool install = false)
        {
            try
            {
                var filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path);
                if (install)
                {
                    if (!File.Exists(path))
                    {
                        using (var file = File.Create(filePath))
                        {
                            _xmlSerializer.Serialize(file, Activator.CreateInstance(typeof(T)));
                        }
                    }
                }
                using (var reader = XmlReader.Create(filePath))
                {
                    Serializer = (T)_xmlSerializer.Deserialize(reader);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
