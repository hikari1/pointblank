﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Database
{
    public interface IEntity
    {
        object Id { get; set; }
    }
}
