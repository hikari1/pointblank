﻿using Commons.Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Database.Repositories
{
    public class AccountRepository : Repository<Account>
    {
        private IDatabaseContext _context { get; }
        public AccountRepository(IDatabaseContext context) : base(context.GetCollection<Account>("accounts"))
        {
            _context = context;
        }
    }
}
