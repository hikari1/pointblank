﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Database
{
    public interface IDatabaseContext
    {
        public IMongoCollection<T> GetCollection<T>(string collection);
    }
}
