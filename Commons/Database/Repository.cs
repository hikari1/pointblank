﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Database
{
    public abstract class Repository<T> : IRepository<T> where T : IEntity
    {
        private IMongoCollection<T> _collection { get; }
        public Repository(IMongoCollection<T> collection)
        {
            _collection = collection;
        }

        public async Task DeleteOneAsync(T entity)
        {
            await _collection.DeleteOneAsync(x => x.Id == entity.Id);
        }

        public async Task DeleteOneAsync(Expression<Func<T, bool>> expression)
        {
            await _collection.DeleteOneAsync(expression);
        }

        public async Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return await _collection.FindSync(expression).FirstOrDefaultAsync();
        }

        public T FirstOrDefault(Expression<Func<T, bool>> expression)
        {
            return _collection.Find(expression).FirstOrDefault();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> expression)
        {
            return await _collection.FindSync(expression).AnyAsync();
        }

        public async Task InsertOneAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task ReplaceOneAsync(T entity)
        {
            await _collection.ReplaceOneAsync(doc => doc.Id == entity.Id, entity);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}
