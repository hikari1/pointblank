﻿using Commons.Xml.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Database
{
    public class DatabaseContext : IDatabaseContext
    {
        private DatabaseSettings _options { get; }
        private ILogger<DatabaseContext> _logger { get; }
        private MongoClient _client { get; }
        private IMongoDatabase _database { get; }
        public DatabaseContext(IOptions<DatabaseSettings> options, ILogger<DatabaseContext> logger)
        {
            _options = options.Value;
            _logger = logger;
            _client = new MongoClient(_options.ConnectionString);
            _database = _client.GetDatabase(_options.Database);
        }

        public IMongoCollection<T> GetCollection<T>(string collection)
        {
            return _database.GetCollection<T>(collection);
        }

    }
}
