﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Database
{
    public interface IRepository<T>
    {
        T FirstOrDefault(Expression<Func<T, bool>> expression);
        Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> expression);
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        Task InsertOneAsync(T entity);

        Task DeleteOneAsync(T entity);

        Task DeleteOneAsync(Expression<Func<T, bool>> expression);

        Task ReplaceOneAsync(T entity);
    }
}
