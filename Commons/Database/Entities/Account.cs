﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Database.Entities
{
    public class Account : IEntity
    {
        [BsonId]
        public object Id { get; set; }
    }
}
