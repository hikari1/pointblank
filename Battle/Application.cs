﻿using Commons.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Commons.Xml;
using Battle.Settings;

namespace Battle
{
    public class Application
    {
        private static ServiceProvider _serviceProvider { get; set; }
        public static Type GetService<Type>() => _serviceProvider.GetService<Type>();
        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().Enrich.FromLogContext().WriteTo.Console().CreateLogger();
            var serviceCollection = new ServiceCollection();
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("Data/settings.json", false).Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<DatabaseSettings>(configuration.GetSection("Database"));

            serviceCollection.AddLogging(configure => configure.AddSerilog());
            serviceCollection.AddScoped<IDatabase, Database>();
            serviceCollection.AddSingleton(new XmlSettings<BattleServer>());
            serviceCollection.AddSingleton(new XmlSettings<BattleCharas>());
            serviceCollection.AddSingleton(new XmlSettings<BattleMaps>());
            serviceCollection.AddSingleton(new XmlSettings<MeleeExceptions>());

            _serviceProvider = serviceCollection.BuildServiceProvider();

            var battleServer = _serviceProvider.GetService<XmlSettings<BattleServer>>();
            var battleCharas = _serviceProvider.GetService<XmlSettings<BattleCharas>>();
            var battleMaps = _serviceProvider.GetService<XmlSettings<BattleMaps>>();
            var meleeExceptions = _serviceProvider.GetService<XmlSettings<MeleeExceptions>>();

            if (battleServer.Load("Data/Battle/Network.xml", true) &&
                battleCharas.Load("Data/Battle/Charas.xml", true) &&
                battleMaps.Load("Data/Battle/Maps.xml", true) &&
                meleeExceptions.Load("Data/Battle/MeleeExceptions.xml", true))
            {
                var logger = _serviceProvider.GetService<ILogger<Application>>();
                logger.LogInformation($"Host: {battleServer.Serializer.HostAddress}:{battleServer.Serializer.HostPort}");
                logger.LogInformation($"Auth Port: {battleServer.Serializer.AuthPort}");
                logger.LogInformation($"Sync Port: {battleServer.Serializer.SyncPort}");
                logger.LogInformation($"Charas: {battleCharas.Serializer.Charas.Count}");
                logger.LogInformation($"Maps: {battleMaps.Serializer.Maps.Count}");
                logger.LogInformation($"Melee Exceptions: {meleeExceptions.Serializer.Weapons.Count}");
            }
            await Task.Delay(-1);
        }
    }
}
