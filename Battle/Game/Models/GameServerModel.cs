﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Battle.Database.Models
{
    public class GameServerModel
    {
        public int _state { get; set; }
        public int _id { get; set; }
        public int _type { get; set; }
        public int _lastCount { get; set; }
        public int _maxPlayers { get; set; }
        public string _ip { get; set; }
        public ushort _port { get; set; }
        public ushort _syncPort { get; set; }
        public IPEndPoint Connection { get; }
        public GameServerModel(string ip, ushort syncPort)
        {
            _ip = ip;
            _syncPort = syncPort;
            Connection = new IPEndPoint(IPAddress.Parse(ip), syncPort);
        }
    }
}
