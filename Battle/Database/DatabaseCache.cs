﻿using Battle.Database.Models;
using Commons.Database;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Battle.Database
{
    public static class DatabaseCache
    {
        public readonly static List<GameServerModel> _servers = new List<GameServerModel>();
        public static void LoadGameServers()
        {
            using (var connection = Application.GetService<IDatabase>().OpenConnection)
            {
                var command = connection.CreateCommand();
                connection.Open();
                command.CommandText = "SELECT * FROM gameservers ORDER BY id ASC";
                command.CommandType = CommandType.Text;
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    var server = new GameServerModel(data.GetString(3), (ushort)data.GetInt32(5))
                    {
                        _id = data.GetInt32(0),
                        _state = data.GetInt32(1),
                        _type = data.GetInt32(2),
                        _port = (ushort)data.GetInt32(4),
                        _maxPlayers = data.GetInt32(6)
                    };
                    _servers.Add(server);
                }
                command.Dispose();
                data.Close();
                connection.Dispose();
                connection.Close();
            }
        }
    }
}
