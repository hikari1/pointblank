﻿using Commons.Models.Settings;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Database
{
    public class Database : IDatabase
    {
        private NpgsqlConnectionStringBuilder _connectionStringBuilder { get; }
        public NpgsqlConnection OpenConnection => new NpgsqlConnection(_connectionStringBuilder.ConnectionString);
        public Database(IOptions<DatabaseSettings> options)
        {
            _connectionStringBuilder = new NpgsqlConnectionStringBuilder
            {
                Database = options.Value.Database,
                Host = options.Value.Host,
                Username = options.Value.Username,
                Password = options.Value.Password,
                Port = options.Value.Port
            };
        }
    }
}
