﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battle.Settings
{
    public class BattleServer
    {
        public string HostAddress { get; set; } = "127.0.0.1";
        public int UdpVersion { get; set; } = 3;
        public ushort HostPort { get; set; } = 40009;
        public int AuthPort { get; set; } = 39190;
        public ushort SyncPort { get; set; } = 1910;
        public ushort MaxDrop { get; set; } = 25;
        public ushort SetHpAll { get; set; } = 100;
        public ushort SetHpGm { get; set; } = 100;
        public bool IsTestMode { get; set; } = false;
        public bool SendInfoToServer { get; set; } = true;
        public bool SendFailMessage { get; set; } = true;
        public bool EnableLog { get; set; } = true;
        public bool UseMaxAmmoInDrop { get; set; } = false;
        public bool UseHitMarker { get; set; } = false;
        public bool IsGranade { get; set; } = true;
        public bool DamageChecker { get; set; } = false;
        public bool BomberBoom { get; set; } = true;
        public bool RotationXYZ { get; set; } = true;
        public bool RotationByteXYZ { get; set; } = false;
        public float PlantDuration { get; set; } = 5.5f;
        public float DefuseDuration { get; set; } = 6.5f;
        public float SecondBullet { get; set; } = 0.1f;
        public float SpeedBullet { get; set; } = 0.2f;
    }
}
