﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Battle.Settings
{
    public class BattleMaps
    {
        public List<Map> Maps { get; set; } = new List<Map>()
        {
            new Map
            {
                Id = 1,
                Objects = new List<Object>()
                    {
                        new Object
                        {
                            Id = 1
                        },
                        new Object
                        {
                            Id = 2
                        }
                    }
                }
        };
    }
    public class Map
    {
        [XmlAttribute]
        public int Id { get; set; }
        [XmlElement]
        public List<Object> Objects { get; set; }
    }
    public class Object
    {
        [XmlAttribute]
        public int Id { get; set; }
        [XmlAttribute]
        public bool NeedSync { get; set; }
        [XmlAttribute]
        public int Anim1 { get; set; }
        [XmlAttribute]
        public int Life { get; set; }
    }
}
