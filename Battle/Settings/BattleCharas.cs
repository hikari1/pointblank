﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Battle.Settings
{
    public class BattleCharas
    {
        public List<Chara> Charas { get; set; } = new List<Chara>();
    }
    public class Chara
    {
        [XmlAttribute]
        public int Id { get; set; }
        [XmlAttribute]
        public int Life { get; set; }
        [XmlAttribute]
        public int Type { get; set; }
    }
}
