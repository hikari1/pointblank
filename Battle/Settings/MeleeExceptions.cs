﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Battle.Settings
{
    public class MeleeExceptions
    {
        public List<Weapon> Weapons { get; set; }
    }
    public class Weapon
    {
        [XmlAttribute]
        public int Slot { get; set; }
        [XmlAttribute]
        public int Number { get; set; }
    }
}
