﻿using Commons.Database;
using Commons.Database.Repositories;
using Commons.Xml.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Auth
{
    public class Application
    {
        private static ServiceProvider _serviceProvider { get; set; }
        public static Type GetService<Type>() => _serviceProvider.GetService<Type>();
        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().Enrich.FromLogContext().WriteTo.Console().CreateLogger();
            var serviceCollection = new ServiceCollection();
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("Data/settings.json", false).Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<DatabaseSettings>(configuration.GetSection("Database"));
            serviceCollection.AddLogging(configure => configure.AddSerilog());
            serviceCollection.AddSingleton<IDatabaseContext, DatabaseContext>();
            serviceCollection.AddTransient<AccountRepository>();
            await Task.Delay(-1);
        }
    }
}
