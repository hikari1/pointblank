﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Auth.Settings
{
    public class BootstrapSettings
    {
        [XmlAttribute]
        public string Host { get; set; }
        [XmlAttribute]
        public int Port { get; set; }
        [XmlAttribute]
        public int Boss { get; set; }
        [XmlAttribute]
        public int Worker { get; set; }
        [XmlAttribute]
        public int BufferSize { get; set; }
        [XmlAttribute]
        public int Backlog { get; set; }
    }
}
