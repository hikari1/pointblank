﻿using Auth.Settings;
using Commons.Network;
using Commons.Network.Bootstrap;
using Commons.Network.Sessions;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Network
{
    public class AuthServerBootstrap : IBootstrap
    {
        private MultithreadEventLoopGroup Boss { get; set; }
        private MultithreadEventLoopGroup Worker { get; set; }
        private ServerBootstrap Bootstrap { get; set; }
        private BootstrapSettings Settings { get; set; }
        public AuthServerBootstrap(IOptions<BootstrapSettings> bootstrapSettings, SessionManager sessionManager, PacketManager packetManager)
        {
            Boss = new MultithreadEventLoopGroup(bootstrapSettings.Value.Boss);
            Worker = new MultithreadEventLoopGroup(bootstrapSettings.Value.Worker);
            Bootstrap = new ServerBootstrap()
                    .Group(Boss, Worker)
                    .Channel<TcpServerSocketChannel>()
                    .ChildHandler(new ActionChannelInitializer<ISocketChannel>(channel =>
                    {
                        var session = new Session(channel, new Cryptography());
                        //channel.Pipeline.AddLast(new PacketEncoder(session.Cryptography));
                        //channel.Pipeline.AddLast(new PacketDecoder(session.Cryptography));
                        channel.Pipeline.AddLast(new NetworkHandler());
                    }))
                    .ChildOption(ChannelOption.AutoRead, true)
                    .ChildOption(ChannelOption.SoKeepalive, true)
                    .ChildOption(ChannelOption.TcpNodelay, true)
                    .ChildOption(ChannelOption.SoBacklog, bootstrapSettings.Value.Backlog)
                    .ChildOption(ChannelOption.SoRcvbuf, bootstrapSettings.Value.BufferSize)
                    .ChildOption(ChannelOption.SoSndbuf, bootstrapSettings.Value.BufferSize);
            Settings = bootstrapSettings.Value;

        }
        public Task Bind()
        {
            return Bootstrap.BindAsync(IPAddress.Any, Settings.Port);
        }

        public Task Shutdown()
        {
            throw new NotImplementedException();
        }
    }
}
